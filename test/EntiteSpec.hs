module EntiteSpec where

import Test.Hspec
import Test.QuickCheck

import qualified Data.Map as M
import qualified System.Random as Rand
import System.IO.Unsafe
import CarteSpec
import Prelude
import Carte
import Entities

{-
    Générateurs d'objets
-}

genObject:: Gen Obj
genObject = do 
    n <- choose(1,3)
    return (randomObj n) 

instance Arbitrary Obj where
    arbitrary = genObject

{-
    Générateur de movements
-}

genMovement :: Gen Movement 
genMovement = do 
    n <- choose (1,5)
    return (randomMov n) 

instance Arbitrary Movement where
    arbitrary = genMovement    
{-
    Générateut d'un joueur
-}

genEntity :: Gen Entity 
genEntity = oneof [
            do  lp <- choose(10,20)
                os <- (listOf genObject) `suchThat` (\x -> Prelude.length x == 10)
                ms <- (listOf genMovement) `suchThat` (\x -> Prelude.length x == 10)
                c <- (genCoord 10 10)
                return (Player lp os ms c), 
--    Générateur de monstre
            do  lp <- choose(10,20)
                os <- listOf genObject `suchThat` (\x -> Prelude.length x == 10)
                ms <- listOf genMovement `suchThat` (\x -> Prelude.length x == 10)
                return (Monster lp os ms )
            ]

instance Arbitrary Entity where
    arbitrary = genEntity



life_potion_prop :: Property 
life_potion_prop = forAll genObject $ lifePotion_inv

entityPotionSpec = do 
    describe "invariant sue le lifePotion " $ do 
        it "respete l'invariant pour tout objet lifePotion sa valeur doit être positif" $ 
            property life_potion_prop 

entity_inv_prop :: Property 
entity_inv_prop = forAll genEntity $ entity_pre 

entityLifeointsSpec = do 
    describe "invariant sue le lifePotion " $ do 
        it "Toute les entités devrainet commencé avec une valeur de lifePoints positive" $ 
            property entity_inv_prop 

entiteSpecs = do
    entityPotionSpec
    entityLifeointsSpec