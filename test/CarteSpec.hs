module CarteSpec where

import Test.Hspec
import Test.QuickCheck
import Carte
import qualified Data.Map as M
import qualified System.Random as Rand
import System.IO.Unsafe

--import qualified Data.Sequence as Seq

{-
    Génération d'une care aléatoire avec une langueure et une largeure aléatoire, mais supérieur à 10 case 
    et une map initialisée aléatoirement avec des contraintes qui sont : 
        1. entourées par des murs 
        2. une seule entrée/une seule sortie
        3. la sortie est accessible depuis l'entrée
-}
genCarte :: Gen Carte 
genCarte = do 
    w <- choose(5,30)
    l <- choose (5,30)
    let map =init_map w l 
    return $ (Carte w l map)

{-
    instancier arbitrary pour Carte
-}

instance Arbitrary Carte where
    arbitrary = genCarte


samples :: Int -> Gen a -> IO [a]
samples n gen = do 
    l <- sample' gen 
    return $ take n l

{-
    Génération de coordonnées aléatoire respectant l'invariant 
    d'être à l'internieur du labyrinthe
-}
genCoord :: Int -> Int -> Gen Coord 
genCoord w l = do 
    x <- choose (0,(w-1))
    y <- choose (0,(l-1))
    return $ (Coord x y)

{-
    tester que toute les coordonnée à l'intérieur d'une carte respecte son invariant
-}
prop_coord_carte_inv :: Property 
prop_coord_carte_inv = forAll genCarte $ carte_inv_coord 

carteSpecCoord = do 
    describe "invariant coordonnéees" $ do 
        it "respete l'invariant coordonnées à l'interieur de la carte" $ 
            property prop_coord_carte_inv 



prop_case_carte_inv :: Carte -> Property 
prop_case_carte_inv carte@(Carte l w map)=
    (carte_pre carte) ==>  forAll (genCoord l w) $ \x ->  (case_exists_inv x carte)

carteSpecCase = do 
    describe "invariant case" $ do 
        it "pour tout coordonnée dans la carte il existe une case correspondant" $ 
            property prop_case_carte_inv 




showCarteSpec = do 
    describe "tester l'affichage d'une carte " $ do
        it "affiche la carte " $ do
            let carte = (Carte 2 2 (M.fromList ([((Coord 0 0),Wall),((Coord 0 1),Wall),((Coord 1 0),Wall),((Coord 1 1),Wall) ]))) in 
                (showCarte carte) `shouldBe` "XX\nXX\n"


carteOkSpec = do 
    describe "chemin sorit ok" $ do 
        it "testet que la sortie est  accebile depuis l'entrée" $ do  
            let carte = (Carte 4 4 (M.fromList[((Coord 0 0),In),((Coord 1 0),Wall),((Coord 2 0),Wall),((Coord 3 0),Wall),
                                      ((Coord 0 1),Vide),((Coord 1 1),Wall),((Coord 2 1),Vide),((Coord 3 1),Vide),
                                      ((Coord 0 2),Vide),((Coord 1 2),Vide),((Coord 2 2),Wall),((Coord 3 2),Wall),
                                      ((Coord 0 3),Wall),((Coord 1 3),Out),((Coord 2 3),Wall),((Coord 3 3),Wall)]))  
            (cheminOutOk carte) `shouldBe` True


{-
    les différents tests à lancer
-}

carteSpecs = do
    carteSpecCoord
    carteSpecCase
    showCarteSpec
    carteOkSpec
