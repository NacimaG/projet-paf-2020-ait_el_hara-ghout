module LevelSpec where

import Test.Hspec
import Test.QuickCheck
import qualified Data.Map as M
import qualified System.Random as Rand
import Carte
import Entities
import Level 
import CarteSpec
import EntiteSpec



-- générteur de Level 

genLevel :: Gen Level 
genLevel = do 
    carte <- genCarte
    let ents =(initEntitisMap 10 10)
    let objs =(initObjMap 10 10)
    let g = (Rand.mkStdGen 10)
    return (Level carte ents objs g)


instance Arbitrary Level where
    arbitrary = genLevel

{-
    Tester que toute les entités présentes dans le jeu ont des point de vie >0
-}

prop_lifep_inv ::  Property 
prop_lifep_inv = forAll genLevel $ level_life_inv

levellifePSpec = do 
    describe "precondition de clef" $ do 
        it "présence d'une clef dans le niveau permettant au joueur d'accéder au niveaux prochain" $ 
            property prop_lifep_inv 


{-
    Tester que l'invaraint de présence de joueur d'au moins un joueur dan sun niveau
    donné passe pour les Level générés

   Présence d'une clé àfin de permettre au joueur d'avancer
-}
prop_levelKey_pre ::  Property 
prop_levelKey_pre = forAll genLevel $ level_key_pre

levelKeySpec = do 
    describe "precondition de clef" $ do 
        it "présence d'une clef dans le niveau permettant au joueur d'accéder au niveaux prochain" $ 
            property prop_levelKey_pre 

{-
    Préécondition de la présence d'une entité avant de la tester
-}

popEntitySpec = do 
    describe "precondition à la suppression d'une entité " $ do
        it "l'netité devrait être présente dans la liste pour pouvoir la supprimer " $ do
            let pl = (Player 10 [] [] (Coord 1 1)) in  
               (popEntPre pl [((Coord 1 1),pl), ((Coord 1 3),(Monster 10 [] []))] ) `shouldBe` True






levelSpecs = do
    levelKeySpec
    popEntitySpec
    levellifePSpec
