module ModelSpec where

import Test.Hspec
import Test.QuickCheck
import qualified Data.Map as M
import qualified System.Random as Rand
import Carte
import Entities
import Level 
import Model
import CarteSpec
import EntiteSpec
import LevelSpec


{-
    Générteur de gameState
        1. avec 4 niveaux 
 
-}


genGS :: Gen GameState 
genGS = do 
        nb <- choose(1,4)
        lvls <- (listOf genLevel) `suchThat` (\x -> Prelude.length x == 4)
        -- on considère 4 entités
        let st = Running
        return (GameState nb lvls st)


instance Arbitrary GameState where
    arbitrary = genGS

{-
    Il y a au moins un niveau dans la liste levels
-}
prop_gameInv ::  Property 
prop_gameInv = forAll genGS $ initGame_inv

modelInvSpec = do 
    describe "invariant de jeu" $ do 
        it "existence d'un niveau au moins" $ 
            property prop_gameInv 


{-
    Dans le jeu on a un etat "Running"
-}

prop_game_pre ::  Property 
prop_game_pre = forAll genGS $ stat_Game_pre

modelGameSpec = do 
    describe "precondition d'un gameState" $ do 
        it "on commence la partie avec un état Running " $ 
            property prop_game_pre 




modelSpecs = do
   modelInvSpec
   modelGameSpec



