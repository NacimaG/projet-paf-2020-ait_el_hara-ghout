import Test.Hspec 
import qualified CarteSpec as CS
import qualified EntiteSpec as ES
import qualified LevelSpec as LS
import qualified ModelSpec as MS 

main :: IO() 
main = hspec $ do
    CS.carteSpecs 
    ES.entiteSpecs
    LS.levelSpecs
    MS.modelSpecs
