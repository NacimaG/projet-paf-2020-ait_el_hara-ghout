{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad (unless)
import Control.Concurrent (threadDelay)

import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.List (foldl')

import Foreign.C.Types (CInt (..) )

import SDL
import SDL.Time (time, delay)
import Linear (V4(..))

import Tools.TextureMap (TextureMap, TextureId (..))
import qualified Tools.TextureMap as TM

import Tools.Sprite (Sprite)
import qualified Tools.Sprite as S

import Tools.SpriteMap (SpriteMap, SpriteId (..))
import qualified Tools.SpriteMap as SM

import Tools.Keyboard (Keyboard)
import qualified Tools.Keyboard as K

import qualified Debug.Trace as T

import Model (GameState)
import qualified Model as M

import Data.Maybe
import qualified System.Random as Rand

import Carte
import Entities
import Level


loadMap :: Renderer -> FilePath -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadMap rdr floorpath wallpath tmap smap = do
  tmap' <- TM.loadTexture rdr floorpath (TextureId "floor") tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "floor") (S.mkArea 0 0 40 40)
  let smap' = SM.addSprite (SpriteId "floor") sprite smap 
  
  tmap'' <- TM.loadTexture rdr wallpath (TextureId "wall") tmap'
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "wall") (S.mkArea 0 0 40 40)
  let smap'' = SM.addSprite (SpriteId "wall") sprite smap' 
  
  tmap' <- TM.loadTexture rdr "assets/images/door_closed_r.png" (TextureId "dcr") tmap''
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "dcr") (S.mkArea 0 0 40 40)
  let smap' = SM.addSprite (SpriteId "dcr") sprite smap'' 
  
  tmap'' <- TM.loadTexture rdr "assets/images/door_closed_l.png" (TextureId "dcl") tmap'
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "dcl") (S.mkArea 0 0 40 40)
  let smap'' = SM.addSprite (SpriteId "dcl") sprite smap' 
  return (tmap'', smap'')

loadSomething :: Renderer-> FilePath -> String -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadSomething rdr path txtid tmap smap = do 
  tmap' <- TM.loadTexture rdr path (TextureId txtid) tmap
  let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId txtid) (S.mkArea 0 0 40 40)
  let smap' = SM.addSprite (SpriteId txtid) sprite smap
  return (tmap', smap')


main :: IO ()
main = do
  initializeAll
  window <- createWindow "Minijeu" $ defaultWindow { windowInitialSize = V2 640 480 }
  renderer <- createRenderer window (-1) defaultRenderer
 
  -- chargement du personnage
  (tmap, smap) <- loadSomething renderer "assets/images/player.png" "perso" TM.createTextureMap SM.createSpriteMap
  (tmap, smap) <- loadSomething renderer "assets/images/monster.png" "monster" tmap smap
  (tmap, smap) <- loadSomething renderer "assets/images/demon.png" "demon" tmap smap
  (tmap, smap) <- loadSomething renderer "assets/images/lifepotion.png" "lifepotion" tmap smap
  (tmap, smap) <- loadSomething renderer "assets/images/torch.png" "torch" tmap smap
  (tmap, smap) <- loadSomething renderer "assets/images/key.png" "key" tmap smap
  (tmap, smap) <- loadMap renderer "assets/images/floor.png" "assets/images/wall.png" tmap smap


  -- initialisation de l'état du jeu
  c1 <- readCarteFromFile  "assets/maps/map1.txt"
  c2 <- readCarteFromFile  "assets/maps/map2.txt"
  let gameState = M.initGameState (initLevels ((Level c1 (Map.fromList []) (Map.fromList []) (Rand.mkStdGen 0)) 
                                              :(Level c2 (Map.fromList []) (Map.fromList []) (Rand.mkStdGen 0)):[]) 
                                              (('M':'K':'P':'P':'M':[]):('M':'P':'P':'M':'M':[]):[])) 
                                              (Coord 0 0 )
  
  
  let kbd = K.createKeyboard

  gameLoop 10 renderer tmap smap kbd gameState


{-
  affiche l'objet identifié par spriteId au coordonnées cx et cy
-}
displaySomething ::  Renderer ->  TextureMap -> SpriteMap -> String -> Coord -> IO ()
displaySomething  renderer tmap smap spriteId (Coord cx cy)=
  do
    S.displaySprite renderer tmap (
      S.moveTo (SM.fetchSprite (SpriteId spriteId) smap)
      (fromIntegral cx*40 ) (fromIntegral cy*40 ) )


displayEntity :: Renderer ->  TextureMap -> SpriteMap -> Entity -> Coord -> IO ()
displayEntity renderer tmap smap (Monster _ _ _) coord = 
  displaySomething renderer tmap smap "monster" coord
displayEntity renderer tmap smap (Player _ _ _ _) coord = 
  displaySomething renderer tmap smap "perso" coord

displayObject :: Renderer ->  TextureMap -> SpriteMap -> Obj -> Coord -> IO ()
displayObject renderer tmap smap (Key) coord = 
  displaySomething renderer tmap smap "key" coord
displayObject renderer tmap smap (LifePotion _) coord = 
  displaySomething renderer tmap smap "lifepotion" coord
displayObject renderer tmap smap (Torch) coord = 
  displaySomething renderer tmap smap "torch" coord

displayCase :: Renderer ->  TextureMap -> SpriteMap -> Case -> Coord -> IO ()
displayCase renderer tmap smap Wall c = 
  displaySomething renderer tmap smap "wall" c
displayCase renderer tmap smap In c = 
  displaySomething renderer tmap smap "dcl" c
displayCase renderer tmap smap Out c = 
  displaySomething renderer tmap smap "dcr" c
displayCase renderer tmap smap _ c = 
  displaySomething renderer tmap smap "floor" c 

{-
  Affichage du contenu de la carte (Murs, Portes, sol ...)
-}
displayMap :: [(Coord,Case)] -> Renderer -> TextureMap -> SpriteMap -> IO ()
displayMap xs renderer tmap smap = 
  mapM_ (\(coord,case_) -> displayCase renderer tmap smap case_ coord) xs

displayLevel :: Level -> Renderer -> TextureMap -> SpriteMap -> IO ()
displayLevel lvl@( Level (Carte l w cont) ents objs stdgen ) renderer tmap smap =
  do
    displayMap  (Map.toList cont) renderer tmap smap
    -- putStrLn ("ici1 -> "++(show (Map.toList ents) ))
    -- putStrLn ("ici2 -> "++(show (Map.toList objs) ))
    mapM_ (\(x,m) -> displayEntity renderer tmap smap m x ) (Map.toList ents) 
    mapM_ (\(x,m) -> displayObject renderer tmap smap m x ) (Map.toList objs) 

displayGameState ::  M.GameState -> Renderer -> TextureMap -> SpriteMap -> IO ()
displayGameState gs@(M.GameState clvl lvls _ ) renderer tmap smap = 
  do
    displayLevel (lvls !! clvl) renderer tmap smap

gameLoop :: (RealFrac a, Show a) => a -> Renderer -> TextureMap -> SpriteMap -> Keyboard -> GameState -> IO ()
gameLoop frameRate renderer tmap smap kbd 
  gameState  = do
  
  startTime <- time
  events <- pollEvents
  let kbd' = K.handleEvents events kbd
  clear renderer

  --affichage de l'état actuel du jeu
  displayGameState gameState renderer tmap smap

  present renderer 
  endTime <- time
  let refreshTime = endTime - startTime
  let delayTime = floor (((1.0 / frameRate) - refreshTime) * 1000)
  threadDelay $ delayTime * 1000 -- microseconds
  endTime <- time
  let deltaTime = endTime - startTime
  let gameState' = M.gameStep gameState kbd' deltaTime

  case (gameState') of 
    (M.GameState _ _ M.Won) -> putStrLn " GAME OVER : YOU WON!\n"
    (M.GameState _ _ M.Lost) -> putStrLn " GAME OVER : YOU LOST!\n"
    (M.GameState _ _ M.Running) -> unless (K.keypressed KeycodeEscape kbd') (gameLoop frameRate renderer tmap smap kbd' gameState')

