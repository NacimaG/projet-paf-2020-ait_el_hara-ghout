module Entities where
import Carte 
import Tools.Keyboard

import qualified System.Random as Rand
import qualified Data.List as L
import qualified Data.Matrix as Mt
import qualified Data.Map as M
import Data.Maybe


data Movement =
  MUp | MDown | MRight | MLeft | Stay deriving (Show, Eq)

data Interaction =
  Collect | Fight deriving Show


{-
  Les objets, ils peuvent etre des armes, des clés, des trésors, des consommables etc
  Des choses que le joueur peut collectionner
  On peut en trouver de façon aléatoire sur la carte
  les monstres peuvent aussi en avoir sur eux, et il faut les tuer pour avoir l'objet
  ex : 
    il faut vaincre un monstre x pour pour avoir la clé de sortie
  ex : 
    LifePotion : recharge la vie de "Int" points au joeur qui la récupére
-}
data Obj =  LifePotion Int
          | Key
          | Torch
  deriving (Show,Eq)


{-
  Les points de vie récupérés sont strictement supérieur à 0 
-}
lifePotion_inv :: Obj -> Bool 
lifePotion_inv (LifePotion x) =  x>0
--  pour les autres objets on met à True
lifePotion_inv _ = True

{-
  Génération d'un objet selon l'entier choisi  aléatoirement entre 1 et 3 
-}
randomObj :: Int -> Obj
randomObj 1 = LifePotion 10
randomObj 2 = Key
randomObj 3 = Torch
{-
  Génération d'un mouvement selon l'entier aléatoirement choisi entre 1 et 5 
-}
randomMov :: Int -> Movement 
randomMov 1 = MUp
randomMov 2 = MDown
randomMov 3 = MRight
randomMov 4 = MLeft
randomMov 5 = Stay


data Entity =
{-
  Le jouer, défini par ses points de vie restant, ses objets collectionnés, 
  les mouvements qu'il va effectuer, et sa destination actuelle
-}   
    Player { 
      lifePoints :: Int,
      objects :: [Obj],
      movements :: [Movement],
      dest :: Coord }
{-
  Des mostres, ils ont des points de vie, une liste d'objets qu'il peuvent avoir sur eux
  et la lsite des mouvements qu'ils vont effectuer
-}
  | Monster { 
      lifePoints :: Int,
      objects :: [Obj],
      movements :: [Movement] }
  deriving (Show)


  
compObjL :: [Obj] -> [Obj] -> Bool
compObjL (x1:t1) (x2:t2) = (x1==x2) && compObjL t1 t2
compObjL [] [] = True
compObjL _ _ = False

compMouvL :: [Movement] -> [Movement] -> Bool
compMouvL (x1:t1) (x2:t2) = (x1==x2) && compMouvL t1 t2
compMouvL [] [] = True
compMouvL _ _ = False

instance Eq Entity where
  (==) (Player lp1 o1 m1 d1) (Player lp2 o2 m2 d2) = (lp1==lp2) && (compObjL o1 o2) && (compMouvL m1 m2) && (d1 == d2)
  (==) (Monster lp1 o1 m1) (Monster  lp2 o2 m2) = (lp1==lp2) && (compObjL o1 o2) && (compMouvL m1 m2) 
  (==) _ _  = False

{-
  compare Ids

eqId :: Int -> Entity -> Bool
eqId id (Player x _ _ _ _) = x == id
eqId id (Monster x _ _ _) = x == id
-}
{-
  Entités précondition: nombre de point de vie >0
  avant de commencer le jeu, et une fois on atteint 0, le joueur a perdu.
-}
entity_pre :: Entity -> Bool 
entity_pre (Player  n _ _ _ ) = n>0
entity_pre (Monster  n _ _) = n>0
{-
  Ajouter un mouvement à une entité
-}
addMovement :: Entity -> Movement -> Entity
addMovement p@(Player  _ _ acts carte_wall_inv) x = p {movements = acts ++ [x]}
addMovement m@(Monster  _ _ acts) x = m {movements = acts ++ [x]}
{-
  Ajouter une une mouvements à une entité
-}
addMovements :: Entity -> [Movement] -> Entity
addMovements p@(Player  _ _ acts _) x = p {movements = acts ++ x}
addMovements m@(Monster  _ _ acts) x = m {movements = acts ++ x}


{-
  Vérifier si une entité donnée est un monstre
-}
isMonster :: Entity -> Bool
isMonster (Monster  _ _ _)= True
isMonster _ = False


{-
  Vérifier si une entité donnée est un joueur
-}
isPlayer :: Entity -> Bool
isPlayer (Player  _ _ _ _)= True
isPlayer _ = False

{-
  Vérifier s'il y a au moins un montre dans la liste
-}
containsMonster :: [Entity] -> Bool
containsMonster [] = False
containsMonster (x:xs) =
    case x of
      Monster  _ _ _ -> True
      _ -> containsMonster xs

{-
  Vérifier si une entité définie par son identifiant est présente dans une liste d'entités

trouve_id_list :: Int -> [Entity] -> Bool
trouve_id_list _ [] = False
trouve_id_list n (x:xs) 
    | eqId n x == True = True   
    | otherwise = trouve_id_list n xs
-}

{-
  Récuperer une entité via son Id si elle est dans la liste, sinon on retourne Nothing

getEntitefromId :: Int-> [Entity] -> Maybe Entity
getEntitefromId n [] = Nothing
getEntitefromId n (x: xs)
    | (eqId n x) == True = Just x
    | otherwise = (getEntitefromId n xs)

-}