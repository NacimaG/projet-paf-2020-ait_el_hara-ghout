module Carte where
    
import Tools.Keyboard (Keyboard)
import qualified Data.Map as M
import qualified Data.List as L
import Data.Maybe
import qualified Data.Matrix as Mt
import qualified System.Random as Rand
import System.IO.Unsafe
import System.IO 

data DoorDir = EO | NS
    deriving Eq
data DoorState = Opened | Closed
    deriving Eq

data Case = Vide        
        | Wall 
        | Door DoorDir DoorState
        | In
        | Out
    deriving Eq 

showCase :: Case -> String
showCase Vide = " "
showCase Wall = "X"
showCase (Door EO Closed) = "-"
showCase (Door NS Closed) = "|"
showCase In = "I"
showCase Out = "O"

instance Show Case where
    show = showCase

readCase :: Char -> Case
readCase ' ' = Vide
readCase 'X' = Wall
readCase '-' = (Door EO Closed)
readCase '|' = (Door NS Closed)
readCase 'I' = In
readCase 'O' = Out 

data Coord = Coord {cx :: Int, cy :: Int}
    deriving (Eq,Show)

compareCoord :: Coord -> Coord -> Ordering
compareCoord (Coord x1 y1) (Coord x2 y2) 
    | x1 < x2                 = LT
    | (x1==x2) && (y1<y2)     = LT
    | (x1==x2) && (y1==y2)    = EQ
    | otherwise               = GT 

instance Ord Coord where
    compare = compareCoord
--data Envi = Envi {contain :: M.Map Coord [Entity]}
--		deriving (Eq, Ord)
data Carte = Carte {
                    width :: Int,
                    length :: Int,
                    content :: (M.Map Coord Case )
                    }

-- invariant des coordonné elles doivent être à l'intérieur du rectangle
coord_inv :: Coord -> Int -> Int -> Bool
coord_inv (Coord x y) w l = (x<w && y<l) && (x>=0 && y>= 0)

-- récuperer les coordonnée d'une case , 
-- notamment dans la recherche de la case d'entrée
getCoord :: Case -> Carte -> Maybe Coord 
getCoord c carte@(Carte w l map) = M.foldrWithKey(\ k v r -> if v==c then (Just k) else r ) Nothing map 

 -- afficher une carte
--showCarte :: Carte -> String
--showCarte (Carte w l map) =  Mt.prettyMatrix (Mt.transpose (Mt.fromList w l (toList map)))


showMap :: [(Coord,Case)] -> Int -> Int-> String
showMap [] _ _ = ""
showMap ((co,c):xs) n y 
    | ( (n`mod`y ) /= 0) = (show c) ++ (showMap xs (n+1) y)
    | otherwise = (show c) ++ "\n"  ++ (showMap xs (n+1) y)
    
-- retourne le contenu d'une Carte (le type) sous forme de String
showCarte :: Carte -> String
showCarte (Carte l w map) = showMap (M.toAscList map) 1 w

    
instance Show Carte where
    show = showCarte


{-
  précondition d'une carte
  taille minimale : avoir au moins 5 ligne et 5 colonne 
-}
carte_pre :: Carte -> Bool 
carte_pre carte@(Carte w l map) = (w>=5 && l >=5) 

-- les coordonnés de la map respectent l'invariant
carte_inv_coord :: Carte -> Bool
carte_inv_coord (Carte w l map) = M.foldrWithKey (\k _ r -> (coord_inv k w l) && r ) True map
{-
   on vérifie que pour une coordonnée qui respecte l'invariant on a 
   toujours une corresendance dans la map
-}
case_exists_inv :: Coord -> Carte -> Bool
case_exists_inv c carte@(Carte w l map) 
    | coord_inv c w l  == True = (M.member c map) == True
    | otherwise = False

-- 
carte_inv_exist :: Carte -> Bool
carte_inv_exist carte@(Carte w l map) = 
    M.foldrWithKey (\k _ r -> (case_exists_inv k carte) && r) True map

-- calculer le nombre d'occurence d'une case dans la carte
frequence_case :: Case -> Carte -> Int
frequence_case c carte@(Carte w l map) = M.foldrWithKey (\k v r -> if v==c  then 1 else 0  + r) 0 map 
{-
   une seule occurence de In et Out
-}
one_In_Out_Inv :: Carte -> Bool
one_In_Out_Inv carte = (frequence_case In carte == 1) && (frequence_case Out carte ==1)

-- entièrement entouré par des murs
-- récupérer les coordonnées qui entourent la carte
coord_wall :: Coord -> Int -> Int -> Bool
coord_wall (Coord x y) w l  
    | (x==0 || x==(w-1))  || (y==0 || y==(l-1)) = True
    | otherwise = False
{- 
  invariant des murs , la carte est entouré par des murs 
-}
carte_wall_inv :: Carte -> Bool
carte_wall_inv (Carte w l map) = M.foldrWithKey (\k v r -> if (coord_wall k w l) == True then v==Wall else True && r) True map
{-
 Définition d'une case aléatoire 
-}
randomCase ::Int -> Int -> Int -> (Coord,Case) 
randomCase i j 1 = ((Coord (i-1) (j-1)), Vide)
randomCase i j 2 = ((Coord (i-1) (j-1)), Wall)
randomCase i j 3 = ((Coord (i-1) (j-1)), (Door EO Closed))
randomCase i j 4 = ((Coord (i-1) (j-1)), (Door NS Closed))
randomCase i j 5 = ((Coord (i-1) (j-1)), In)
randomCase i j 6 = ((Coord (i-1) (j-1)), Out)

---initialisation d'une à des cases aléatoire, et des murs aux extremités
init_map :: Int -> Int -> M.Map Coord Case 
init_map w l = M.fromList (Mt.toList (Mt.matrix w l $ \(i,j) ->
    let g = Rand.mkStdGen 4 in 
     let (r,v) = (Rand.randomR (1,2) g) in if (i,j) == (2,2) then ((Coord (i-1) (j-1)), In) else if (coord_wall (Coord (i-1) (j-1) ) w l ) then 
         ((Coord (i-1) (j-1) ),Wall) else (randomCase i j r ) ))



-- initialisation d'une Carte avec des cases aléatoires à l'interieru et entourné de mur
init_carte :: Int -> Int -> Carte 
init_carte w l = (Carte w l (init_map w l) )

-- tester si c'est une case vide
isVide :: Int -> Int -> (M.Map Coord Case) -> Bool
isVide x y map = M.lookup (Coord x y) map ==( Just Vide)

-- case mur
isWall :: Int -> Int -> (M.Map Coord Case) -> Bool
isWall x y map = M.lookup (Coord x y) map ==( Just Wall)
--
isEOdoor_inv :: Case -> Int -> Int -> (M.Map Coord Case)-> Bool
isEOdoor_inv (Door EO _) x y map = (isWall x (y-1) map ) && (isWall x (y+1) map)
--
isNSdoor_inv :: Case -> Int -> Int -> (M.Map Coord Case)-> Bool
isNSdoor_inv (Door NS _) x y map = (isWall (x-1) y map) && (isWall (x+1) y map)
--
isEOdoor :: Case -> Bool
isEOdoor (Door EO _) = True
isEOdoor _ = False

isNSdoor :: Case -> Bool
isNSdoor (Door NS _) = True
isNSdoor _ = False

-- vérifier si une case est une entrée
isEntrance :: Case -> Bool
isEntrance In = True
isEntrance _ = False

-- vérifier si une case est une sortie
isExit :: Case -> Bool
isExit Out = True
isExit _ = False

-- portes encadrés par des murs
porte_encadre_inv :: Carte -> Bool
porte_encadre_inv (Carte _ _ map)= M.foldrWithKey (\(Coord x y) v r -> if isEOdoor v then (isEOdoor_inv v x y map)
                                                                 else if  isNSdoor v then (isNSdoor_inv v x y map)
                                                                 else True) True map


--
markedMap :: M.Map Coord Case -> M.Map Coord (Case,Bool)
markedMap map =  M.mapWithKey(\k v -> (v,False)) map 
{-
    Marquer la map d'une carte, pour ne as les parcourir plus d'une fois dans les foncitons
-}

markCoord :: Coord -> M.Map Coord (Case,Bool) -> M.Map Coord (Case,Bool)
markCoord co map = case (M.lookup co map ) of 
    Just (caseC,False) -> M.insert co (caseC,True) map 
    Nothing -> error "page non existante"

{- 
    Les fonctions suivantes nous montrent s'il est possible de se déplacer à partie d'une 
    certaine coordonnée dans la map , en vérifiant : 
        1. La coordonnée suivante (Nord,Est,Ouest,Sud) n'est pas un murn ,'est pas vide
        2. Qu'on a pas déjà parcouru cette carte (pour éviter une boucle infinie on visite une case
                une seule fois)
-}
northCaseOk :: Coord -> Carte ->M.Map Coord (Case,Bool)->  Bool 
northCaseOk co@(Coord x y) cate@(Carte w l map) mapMark = case (coord_inv (Coord x (y-1)) w l ) of 
    False -> False 
    True -> case M.lookup (Coord x (y-1)) mapMark of 
        Just (_,True) -> False 
        Just (Wall,_) -> False
        Just (_,False) -> True 
        Nothing -> False

southCaseOk :: Coord -> Carte ->M.Map Coord (Case,Bool) ->  Bool 
southCaseOk (Coord x y) cate@(Carte w l map) mapMark = case (coord_inv (Coord x (y+1)) w l ) of 
    False -> False 
    True -> case M.lookup (Coord x (y+1)) mapMark of 
        Just (_,True) -> False 
        Just (Wall,_) -> False
        Just (_,False) -> True
        Nothing -> False

estCaseOk :: Coord -> Carte -> M.Map Coord (Case,Bool) -> Bool 
estCaseOk (Coord x y) cate@(Carte w l map) mapMark = case (coord_inv (Coord (x-1) y) w l ) of 
    False -> False 
    True ->  case M.lookup (Coord (x-1) y) mapMark of 
        Just (_,True) -> False 
        Just (Wall,_) -> False
        Just (_,False) -> True
        Nothing -> False 

ouestCaseOk :: Coord -> Carte -> M.Map Coord (Case,Bool) -> Bool 
ouestCaseOk (Coord x y) cate@(Carte w l map) mapMark= case (coord_inv (Coord (x+1) y) w l ) of 
    False -> False 
    True -> case M.lookup (Coord (x+1) y) mapMark of 
        Just (_,True) -> False 
        Just (Wall,_) -> False
        Just (_,False) -> True
        Nothing -> False

{-
     La fonction permettant de  s'assurer qu'une fois dans le jeu y a youjours une sortie,
     ce qui rend le jeu possible, et que la sortie est unique
     
-}
outOK ::Coord -> Carte -> M.Map Coord (Case,Bool) -> Bool
outOK co@(Coord x y) mcarte mapMark =  
    case (northCaseOk co mcarte mapMark) of 
        False ->  case (southCaseOk co mcarte mapMark) of 
            False-> case(estCaseOk co mcarte mapMark) of 
                False -> case (ouestCaseOk co mcarte mapMark) of 
                    False -> False
                    True ->  case (M.lookup (Coord (x+1) y) mapMark) of 
                        Just (Out,_) -> True 
                        Just (Vide,False)->  outOK (Coord (x+1) y) mcarte (markCoord co mapMark)
                        _ -> False
                True ->  case (M.lookup (Coord (x-1) y) mapMark) of 
                        Just (Out,_) -> True 
                        Just (Vide,False)->  outOK (Coord (x-1) y) mcarte (markCoord co mapMark)
                        _ -> False
            True ->  case (M.lookup (Coord x (y+1)) mapMark) of 
                        Just (Out,_) -> True
                        Just (Vide,False)->  outOK (Coord x (y+1)) mcarte (markCoord co mapMark) 
                        _ -> False
        True ->  case (M.lookup (Coord x (y-1)) mapMark) of 
                        Just (Out,_) -> True
                        Just (Vide,False)->  outOK (Coord x (y-1)) mcarte (markCoord co mapMark) 
                        _ -> False
{-                          
    verifer que à chaque entrée dans le labyrinthe y a bien un chemin qui mène à la sortie
-}
cheminOutOk :: Carte -> Bool 
cheminOutOk carte@(Carte w l map ) = case (getCoord In carte ) of 
    Just co -> (outOK co carte (markedMap map)) 
    Nothing -> False
cheminOutOk _ = False 

-- get case à partir des coordonnées
getCase :: Int -> Int -> (M.Map Coord Case) ->Maybe Case
getCase x y map =  M.lookup (Coord x y) map

-------------------------------------

-- séparer une string par un caractère donné
splite :: Char -> String -> [String]
splite c [] = [""]
splite c (h:t)  | h==c = "":rest
                | otherwise = (h : head rest) : tail rest
        where rest = splite c t

-- construire un élément à partir de coordonée et le caractère décrivant la carte
makeElem:: Int -> Int -> Char -> (Coord,Case)
makeElem n m c = ((Coord n m),(readCase c) )
-- construire une ligne
makeLine :: String -> Int -> Int ->[(Coord,Case)]
makeLine "" _ _ = []
makeLine (x:xs) n m = (makeElem n m x ): (makeLine xs (n+1) m ) 

-- construire la matrice représentant la carte
makeMatrix :: [String]-> Int -> Int -> [(Coord,Case)]
makeMatrix [] _ _ = []
makeMatrix (x:xs) n m = L.union (makeLine x n m)  (makeMatrix xs n (m+1)) 

-- lecture d'une carte
readCarte :: String -> Carte
readCarte "" = (Carte 0 0 M.empty)  
readCarte str = let (x:xs)= (splite '\n' str) in
    let len = L.length (x:xs) in
        let width = (L.length x) in
            (Carte width len (M.fromList (makeMatrix (x:xs) 0 0)))

--lis une carte d'un fichier 
readCarteFromFile :: FilePath -> IO Carte
readCarteFromFile fp = do 
    contents <- readFile fp 
    return (readCarte contents)


--affiche une carte
showCarteContent :: Carte -> IO ()
showCarteContent gs@(Carte w l c) = let tmpf (Coord x y) caze res = res++(show x)++" "++(show y)++" "++(show caze)++"\n" in
     putStrLn $ (show w) ++ " " ++  (show l) ++ " " <> (M.foldrWithKey tmpf "\n\n" c)
-------------------------------------

{-
    Récupérer les coordonnées de la case d'entrée dans la carte
-}
getInDoorCoord :: [(Coord,Case)] -> Coord
getInDoorCoord ((coo,ca):t) 
  | isEntrance ca = coo
  | otherwise = getInDoorCoord t
getInDoorCoord [] = error "getInDoorCoord : NO ENTRACE!"



