module Level where

import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.Matrix as Mt
import qualified System.Random as Rand

import Carte 
import Entities


{-
  Un niveau, défini par sa carte, la liste des monstres qui trainent sur ce niveau,
  et la liste des objets qu'on peut collectionner sur ce niveau

  pre : le niveau est dans un état cohérent, 
    (pas d'entités sur les cases ifranchissable ou en dehors de la carte)
  post : la meme chose
-}
data Level = Level { 
              gMap :: Carte,
              entities :: M.Map Coord Entity,
              objectsLvl :: M.Map Coord Obj,
              stdgen :: Rand.StdGen  
            }
  deriving (Show)
{-
  Nombre de joueur dans un niveau
-}
nbPlayerLev :: Level -> Int 
nbPlayerLev l@(Level _ entities _ _) = M.foldrWithKey(\k v r -> if (isPlayer v) then 1 else 0 + r ) 0 entities


nbPlLevsOk :: [Level] -> Bool
nbPlLevsOk [] = True
nbPlLevsOk (x:xs) = (nbPlayerLev x == 1) && (nbPlLevsOk xs)
{-
  LEs entités présente dan sle jeu : Joueur , Monstre, 
    1. Avoir des points de vie suffisantss >0
-}

gEnt_inv :: Entity -> Bool 
gEnt_inv (Player x _ _ _ ) = (x>0)
gEnt_inv (Monster x _ _) = (x>0)

{-
  Tester que toute les entités vivantes ont des point de vie > 0
-}

level_life_inv :: Level -> Bool 
level_life_inv lvl@(Level _ ents _ _)= M.foldrWithKey(\k v r -> (gEnt_inv v) && r) True ents 


{-
  Level préconditions :
  1. Il y a une clé dans la liste des objets (pour passer au niveau suivant)
-}

level_key_pre :: Level -> Bool 
level_key_pre (Level _ _ objs _)= M.foldrWithKey(\k v r -> if v == Key then True else False || r) False objs

{-
  Retourne une liste de coordonnées de cases libres 
  sans entité ni murs
-}
getFreeSpots :: Level -> [Coord]
getFreeSpots lvl@(Level gmap@(Carte _ _ m) ents _ _) =
    L.foldr (\x l-> 
      case (M.lookup x ents) of
        Just _ -> l
        Nothing -> x:l) []
        (M.foldWithKey (\k v ks -> 
          case v of
            Wall -> ks
            _ -> k:ks ) [] m)
{-  
   niveau accessible 
-}
isAccessible :: Level -> Coord -> Bool
isAccessible lvl@( Level gmap@(Carte _ _ field) _ _ _) c =
  case (M.lookup c field) of
    Just Wall -> False
    _ -> True

{-
  verifie s'il y a une Entity dans un coordonnée donnée
-}
containsEntity :: Level -> Coord -> Bool
containsEntity lvl@( Level _ ents _ _) c =
  case (M.lookup c ents) of
    Nothing -> False
    _ -> True

{-
  insére une entité dans la carte du niveau,
  Retourne le nouveau niveau avec l'entité inserée, ou rien si elle est occupée
-}
insertEntity :: Coord -> Level -> Entity -> Level
insertEntity c lvl@(Level m ents objs stdgen) ent = 
  case (M.lookup c ents) of
    Just x ->  (Level m (M.insert c ent ents) objs stdgen) --Modifier
    Nothing -> (Level m (M.insert c ent ents) objs stdgen)

{-
  PostCondition: après insertion d'un élément la taille de la map incrément de 1
-}
insertEnityPost :: Coord -> Level -> Entity -> Bool 
insertEnityPost c lvl@(Level _ ents _ _) en = let lvp@(Level _ entp _ _ ) = (insertEntity c lvl en) in 
    (L.length (M.toList entp)) == ((L.length(M.toList ents))+1 )

{-
  PreCondition: 1.la taille de la liste >0 
                2.l'entité à supprimer est contenu dans la liste
-}

popEntPre :: Entity -> [(Coord,Entity)] -> Bool 
popEntPre _ [] = False
popEntPre en ((_,x):xs)  = if  x==en then  True else (popEntPre en xs)

popEntity_ :: Entity -> [(Coord, Entity)] -> [(Coord, Entity)]
popEntity_ ent ((c,e):t) 
  | ent==e = t
  | otherwise = (c,e):(popEntity_ ent t)
popEntity_ _ [] = []

{-
  Suppression d'une entité de la carte
-}
popEntity :: Entity -> Level -> Level
popEntity ent lvl@(Level _ ents _ _) = lvl { entities = M.fromList (popEntity_ ent (M.toList ents)) }

{-
  PostCondidtion : 
    Après suppression d'une entitén elle y est plus dans la liste et la taillle = taille -1


-}

{-
  pre : le jouer est sur le niveau
  post : rien
-}
getPlayer :: [(Coord,Entity)] -> Maybe (Coord,Entity)
getPlayer ((c,ent):t) =  
  case ent of
    (Player _ _ _ _) -> Just (c,ent)
    _ -> getPlayer t
getPlayer [] = Nothing

{-
  récupérer un joueur d'un niveau donné
-}
getPlayerFromLevel :: Level -> Maybe (Coord,Entity)
getPlayerFromLevel (Level _ ents _ _) = getPlayer (M.toList ents)
{-
  Récupere la carte du niveau
-}

getCarteMap :: Level -> M.Map Coord Case
getCarteMap (Level (Carte _ _ gmap) _ _ _ ) = gmap


{-
   initialiser une map d'entité coordonnées en mettant un joueur, et 3 monstres 
   - à utiliser pour tester le module Level
-}
initEntitisMap :: Int -> Int -> M.Map Coord Entity 
initEntitisMap w l = M.fromList [((Coord 1 1),(Player 10 [] [] (Coord 1 1))),
                                  ((Coord 3 4 ),(Monster 10 [] []) ),
                                   ((Coord (w-2) (l-3)),(Monster  10 [] [])),
                                    ((Coord (w-1) (l-1)),(Monster  10 [] []))
                                ]
    
{-
   initialiser une map d'entité coordonnées en mettant un joueur, et 3 monstres 
   à utiliser pour tester le module Level
-}
initObjMap :: Int -> Int -> M.Map Coord Obj 
initObjMap w l = M.fromList [((Coord 2 2), Torch ),
                                  ((Coord 3 4 ), Key ),
                                   ((Coord (w-2) (l-3)),(LifePotion 20)),
                                    ((Coord (w-1) (l-1)),(LifePotion 20))
                                ]
   
{-
  Insérer une entité dans une case aléatoire
-}
insertEntityInRandomCell :: Level -> Entity -> (Int,Rand.StdGen) -> [Coord] ->  Level 
insertEntityInRandomCell lvl@(Level (Carte w l cont) ents objs stdgen ) ent (p,newstdgen) (h:t)=
  (Level (Carte w l cont) ( M.insert ((h:t) !! p) ent ents ) objs newstdgen )
insertEntityInRandomCell lvl@(Level _ _ _ _) _ _ _ = lvl

{-
  Ajouter une cellule dans un case aléatoire
-}

addEntityInRandomCell :: Level -> Entity -> Level
addEntityInRandomCell lvl@(Level (Carte w l cont) ents objs stdgen ) ent =
  case (getFreeSpots lvl) of
    (h:t) -> insertEntityInRandomCell lvl ent (Rand.randomR (0, (L.length (h:t))-1) stdgen) (h:t)
    [] -> lvl
addEntityInRandomCell lvl@(Level _ _ _ _) _ = lvl

{-
  Insérer un objet dan sune case aléatoire
-}

insertObjectInRandomCell :: Level -> Obj -> (Int,Rand.StdGen) -> [Coord] ->  Level 
insertObjectInRandomCell lvl@(Level (Carte w l cont) ents objs stdgen ) obj (p,newstdgen) (h:t)=
  (Level (Carte w l cont) ents ( M.insert ((h:t) !! p) obj objs ) newstdgen )
insertObjectInRandomCell lvl@(Level _ _ _ _) _ _ _ = lvl
{-
  Ajouter un objet dans une case aléatoire
-}

addObjectInRandomCell :: Level -> Obj -> Level
addObjectInRandomCell lvl@(Level (Carte w l cont) ents objs stdgen ) obj =
  case (getFreeSpots lvl) of
    (h:t) -> insertObjectInRandomCell lvl obj (Rand.randomR (0, (L.length (h:t))-1) stdgen) (h:t)
    [] -> lvl
addObjectInRandomCell lvl@(Level _ _ _ _) _ = lvl

{-
  Ajout d'entité et d'objets aléatoirement dans un niveau 
-}
addEntitiesAndObjectsToLevel :: Level -> [Char] -> Level
addEntitiesAndObjectsToLevel lvl@(Level (Carte w l cont) ents objs stdgen ) ('M':t) =
  addEntitiesAndObjectsToLevel (addEntityInRandomCell lvl (Monster  100 [] [])) t
addEntitiesAndObjectsToLevel lvl@(Level (Carte w l cont) ents objs stdgen ) ('P':t) =
  addEntitiesAndObjectsToLevel (addObjectInRandomCell lvl (LifePotion 100)) t
addEntitiesAndObjectsToLevel lvl@(Level (Carte w l cont) ents objs stdgen ) ('K':t) =
  addEntitiesAndObjectsToLevel (addObjectInRandomCell lvl (Key)) t
addEntitiesAndObjectsToLevel lvl@(Level (Carte w l cont) ents objs stdgen ) ('T':t) =
  addEntitiesAndObjectsToLevel (addObjectInRandomCell lvl (Torch)) t
addEntitiesAndObjectsToLevel lvl _ = lvl

{-
  Initialisation d'une liste de niveaux
-}
initLevels :: [Level] -> [[Char]] -> [Level]
initLevels (h1:t1) (h2:t2) = ((addEntitiesAndObjectsToLevel h1 h2):(initLevels t1 t2))
initLevels [] [] = []
initLevels _ _ = error "initLevels: les deux listes ne sont pas de meme taille"

{-
data Obj = Thing 
          | LifePotion Int
          | Key

  | Monster { 
      id :: Int,
      lifePoints :: Int,
      objects :: [Obj],
      movements :: [Movement] }


data Envi = Envi {contain :: M.Map Coord [Entity]}
        deriving ( Show)

-- environement initial 
init_envi:: Carte -> Envi 
init_envi carte@(Carte w l map) = (Envi (M.fromList (Mt.toList (Mt.matrix w l $ \(i,j) -> ((Coord (i-1) (j-1)),[])))))

--recupere l'element présent dans une coordonnée dans l'environement
getElemEnv :: Coord -> Envi -> Maybe [Entity]
getElemEnv c (Envi map) = M.lookup c map

franchissable_env ::  Coord -> Envi -> Bool
franchissable_env cor map =
    case (getElemEnv cor map) of
      Nothing -> True
      Just x -> containsMonster x
      --otherwise = containsMonster (fromJust (getElemEnv cor map)) 
-}


{-
rm_env_id :: Int -> Envi -> Envi 
rm_env_id n env@(Envi map)= case (trouve_id n env) of 
    Just (co,ent) -> let l = (M.lookup co map ) in 
        case l of 
            Just (x:xs) -> (Envi (M.insert co (L.delete ent (x:xs)) map))  
            Nothing ->  env 
    Nothing -> env
    
-- insertion d'une nouvelle entité dans la map
inserteEntite :: Coord -> Entity -> Envi -> Envi
inserteEntite c en (Envi map) = case (M.lookup c map) of 
    Nothing -> (Envi (M.insert c [en]  map))
    Just tmp-> (Envi (M.insert c (L.insert en tmp)  map) )        

-- bouger une entité
bouge_id :: Int -> Coord -> Envi -> Envi
bouge_id id co env@(Envi map) = case (trouve_id id env) of 
    Just (cord, ent) -> let envd@(Envi mapd) = (rm_env_id id env) in case (M.lookup co map) of 
        Nothing -> (Envi (M.insert co [ent]  mapd))
        Just l->  (Envi (M.insert co (L.insert ent l)  mapd) )  
    Nothing -> env 


rm_env_id :: Int -> Envi -> Envi 
rm_env_id n env@(Envi map)= case (trouve_id n env) of 
    Just (co,ent) -> let l = (M.lookup co map ) in 
        case l of 
            Just (x:xs) -> (Envi (M.insert co (L.delete ent (x:xs)) map))  
            Nothing ->  env 
    Nothing -> env
    
-- insertion d'une nouvelle entité dans la map
inserteEntite :: Coord -> Entity -> Envi -> Envi
inserteEntite c en (Envi map) = case (M.lookup c map) of 
    Nothing -> (Envi (M.insert c [en]  map))
    Just tmp-> (Envi (M.insert c (L.insert en tmp)  map) )        

-- bouger une entité
bouge_id :: Int -> Coord -> Envi -> Envi
bouge_id id co env@(Envi map) = case (trouve_id id env) of 
    Just (cord, ent) -> let envd@(Envi mapd) = (rm_env_id id env) in case (M.lookup co map) of 
        Nothing -> (Envi (M.insert co [ent]  mapd))
        Just l->  (Envi (M.insert co (L.insert ent l)  mapd) )  
    Nothing -> env 

--data StdGen = StdGen
--    deriving 
-- Modele
-}


{-
data Modele = Cont {    
                    carte :: Carte,
                    envi :: Envi, 
                    log :: String,
                    keyboard :: Keyboard
                    }

--                     gene :: Rand.StdGen


bouge :: Modele -> Entity -> Coord -> Modele
bouge model@(Cont carte env@( Envi map) log k ) ( Player id pvie) co = case trouve_id id env of 
    Nothing -> model
    Just (coord,entity)-> (Cont carte (bouge_id id co env) log k )
bouge model@(Cont carte env@( Envi map) log k ) ( Monster (Monster1 id pvie)) co = case trouve_id id env of 
    Nothing -> model
    Just (coord,entity)-> (Cont carte (bouge_id id co env) log k )
bouge model@(Cont carte env@( Envi map) log k ) ( Mob id pvie) co = case trouve_id id env of 
    Nothing -> model
    Just (coord,entity)-> (Cont carte (bouge_id id co env) log k )
bouge model@(Cont carte env@( Envi map) log k ) ( Tresor id ) co = case trouve_id id env of 
    Nothing -> model
    Just (coord,entity)-> (Cont carte (bouge_id id co env) log k )
   
data Ordre = N | S | E | O | U | R 
    deriving (Show)

-- récupérer la coordonnée au nord d'une coordonnée
northCoord :: Coord -> Int -> Int -> Coord 
northCoord c@(Coord x y ) w l=case (coord_inv (Coord x (y+1)) w l) of
  True -> Coord x (y+1)
  False -> c
-- récupérer la coordonnée au nord d'une coordonnée
southCoord :: Coord -> Int -> Int -> Coord 
southCoord c@(Coord x y ) w l=case (coord_inv (Coord x (y-1)) w l) of
  True -> (Coord x (y-1)) 
  False -> c
-- récupérer la coordonnée au nord d'une coordonnée
estCoord :: Coord -> Int -> Int -> Coord 
estCoord c@(Coord x y ) w l=case (coord_inv (Coord (x-1) y) w l) of
  True -> (Coord (x-1) y)
  False -> c
-- récupérer la coordonnée à l'ouest d'une coordonnée
ouestCoord :: Coord -> Int -> Int -> Coord 
ouestCoord c@(Coord x y ) w l=case (coord_inv (Coord (x+1) y) w l) of
  True -> (Coord (x+1) y) 
  False -> c

findEntite :: M.Map Coord [Entity] -> Entity -> Maybe Coord
findEntite map en = M.foldrWithKey(\k v r -> if (elem en v) == True then Just k else Nothing) Nothing map

-- à revoir 
decide :: [(Int,Ordre)] -> Modele -> Entity -> Modele
decide [] model en = model 
decide list model@(Cont carte@(Carte w l mapCase) env@(Envi map) log k  ) entity = case findEntite map entity of 
  Nothing -> model
  Just co -> let (r,_) = Rand.randomR (0,(L.length list)) $ Rand.mkStdGen (L.length list) in
    case (list!!r) of 
      (_,N) ->  bouge model entity $ northCoord co w l
      (_,S) ->  bouge model entity $ southCoord co w l
      (_,E) ->  bouge model entity $ estCoord co w l
      (_,O) ->  bouge model entity $ ouestCoord co w l
      (_,R) -> model 
      (_,U) -> case entity of 
        (Player id pl ) -> model -- do something
        _ -> model

-- verifier que le montre peut bien se déplacer au nord
northOk :: Coord -> Modele -> Bool 
northOk c model@(Cont carte@(Carte w l mapCase) env@(Envi map) log k )= let co = (northCoord c w l) in
    if (co == c ) then False 
    else if ((M.lookup co mapCase) == (Just Vide)) && (franchissable_env co env) then True else False 
-- verifier que le montre peut bien se déplacer au sud
southOk :: Coord -> Modele -> Bool 
southOk c model@(Cont carte@(Carte w l mapCase) env@(Envi map) log k )= let co = (southCoord c w l) in
    if (co == c ) then False 
    else if ((M.lookup co mapCase) == (Just Vide)) && (franchissable_env co env) then True else False
-- verifier que le montre peut bien se déplacer à l'est
estOk :: Coord -> Modele -> Bool 
estOk c model@(Cont carte@(Carte w l mapCase) env@(Envi map) log k )= let co = (estCoord c w l) in
    if (co == c ) then False 
    else if ((M.lookup co mapCase) == (Just Vide)) && (franchissable_env co env) then True else False
-- verifier que le montre peut bien se déplacer à l'ouest
ouestOk :: Coord -> Modele -> Bool 
ouestOk c model@(Cont carte@(Carte w l mapCase) env@(Envi map) log k )= let co = (ouestCoord c w l) in
    if (co == c ) then False 
    else if ((M.lookup co mapCase) == (Just Vide)) && (franchissable_env co env) then True else False

-- décrire les mouvements possibles par un monstre
prevoit_monster :: Modele -> Entity -> [(Int,Ordre)]
prevoit_monster model@(Cont carte env log k ) (Monster1 id pl ) =
  case trouve_id id env of 
    Just (co,(Monster1 id pl)) -> case (northOk co model) of 
        True -> case (southOk co model ) of 
            True -> case (estOk co model) of 
                True -> case (ouestOk co model) of 
                    True  -> [(1,N),(1,S),(1,E),(1,O),(2,R)]
                    False -> [(1,N),(1,S),(1,E),(2,R)]
                False -> case (ouestOk co model) of 
                    True ->  [(1,N),(1,S),(1,O),(2,R)]
                    False -> [(1,N),(1,S),(2,R)]
            False -> case (estOk co model) of 
                True -> case (ouestOk co model) of 
                    True  -> [(1,N),(1,E),(1,O),(2,R)]
                    False -> [(1,N),(1,E),(2,R)]
                False -> case (ouestOk co model) of 
                    True ->  [(1,N),(1,O),(2,R)]
                    False -> [(1,N),(2,R)]
        False ->case (southOk co model ) of 
            True -> case (estOk co model) of 
                True -> case (ouestOk co model) of 
                    True  -> [(1,S),(1,E),(1,O),(2,R)]
                    False -> [(1,S),(1,E),(2,R)]
                False -> case (ouestOk co model) of 
                    True ->  [(1,S),(1,O),(2,R)]
                    False -> [(1,S),(2,R)]
            False -> case (estOk co model) of 
                True -> case (ouestOk co model) of 
                    True  -> [(1,E),(1,O),(2,R)]
                    False -> [(1,E),(2,R)]
                False -> case (ouestOk co model) of 
                    True ->  [(1,O),(2,R)]
                    False -> [(2,R)]
    Nothing -> []
      
-- définir les coordonnées initiales aléatoire pour un montre 
 

initMonster :: Carte -> (Int,Int) 
initMonster carte@(Carte w l map)= let x = (unsafePerformIO (Rand.getStdRandom (Rand.randomR (1, (w-2))))) in
    let y = (unsafePerformIO (Rand.getStdRandom (Rand.randomR (1, (l-2))))) in 
        case (getCase x y map) of 
            Just c -> if (isVide x y map ) then (x,y) else (initMonster carte )
            Nothing -> error "coordonnees incorrectes"


-}
