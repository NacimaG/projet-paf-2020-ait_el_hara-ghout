
module Model where

import Data.List as L
import Tools.Keyboard (Keyboard)
import qualified Tools.Keyboard as K
import qualified Data.Map as M
import qualified Data.Set as S

import qualified System.Random as Rand
import System.IO.Unsafe
import System.IO

import SDL

import Carte
import Level
import Entities



data State = Won 
            | Lost
            | Running
  deriving (Show)


{-
  Etat du jeu, le niveau actuel, la liste des niveaux, et le joueur

  pre : Un seul joueur dans tous les niveaux, et tous les niveaux sont "cohérents"

  post : la meme chose aprés les déplacements
-}


data GameState = GameState { 
                  currentLevel :: Int, 
                  levels :: [Level],
                  state :: State
                } 
  deriving (Show)



{-
  on commence le jeu avec etat running (no won et non lost)
-}
stat_Game_pre :: GameState -> Bool 
stat_Game_pre gs@(GameState _ _ Running ) = True
stat_Game_pre gs@(GameState _ _ _ ) = False 

{-
  on termine le jeu avec soit un état won soit lost
-}

stat_Game_post :: GameState -> Bool 
stat_Game_post gs@(GameState _ _ Running) = False
stat_Game_post gs@(GameState _ _ _ ) = True 

{-
  Initialisation de l'état de jeu
-}
initGameState :: [Level] -> Coord ->  GameState    
initGameState ( (Level x ents objs stdgen):lvs) coord  =
  GameState 0 ( (Level x ( 
    M.insert coord (Player 100 [] [] (Coord 5 5)) ents
    ) objs stdgen):lvs )  Running 


{-
  game init pre: au moins un niveau pour commencer le jeu
-}
initGame_inv :: GameState -> Bool 
initGame_inv gs@(GameState _ lvls _)= (L.length lvls) > 0


{-
    2. l'invariant sur les coordonnées à l'interieur de la carte  
-}
moveEntityCoord_inv :: Coord -> Level -> Bool 
moveEntityCoord_inv c lv@(Level cart@(Carte l w _) _ _ _) =
  ( coord_inv c l w ) 


{-
  Interaction entre deux entités
-}
entityInteraction :: Level -> Coord -> Entity -> Coord -> Entity -> Level
entityInteraction lvl@(Level _ ents objs _) c1 pl@(Player lp1 l1 _ _)  c2 m@(Monster lp2 l2 _) 
  
  | lp1 > (lp2 `div` 2) =
    case (M.lookup c2 objs) of
      Just x -> 
        lvl { entities = M.delete c1 (M.insert c2 (pl { lifePoints = lp1 - (lp2 `div` 2), 
          objects = (x:(l1++l2))}) ents), objectsLvl = M.delete c2 objs }
      Nothing -> lvl { entities = M.delete c1 (M.insert c2 (pl { lifePoints = lp1 - (lp2 `div` 2), 
          objects = (l1++l2) }) ents) }
  
  | lp1 == (lp2 `div` 2) =
    lvl {entities = M.insert c1 (pl {lifePoints = 1}) (M.insert c2 (m {lifePoints = 1}) ents)}
  
  | otherwise =
    lvl {entities = M.delete c1 ents}

entityInteraction lvl@(Level _ ents _ _) c1 m@(Monster lp1 l1 _) c2 pl@(Player lp2 l2 _ _)
  | lp1 > (lp2 * 2) =
    lvl {entities = M.delete c1 (M.insert c2 (m {lifePoints = lp1 - (lp2 * 2)}) ents)}
  | lp1 == (lp2 * 2) =
    lvl {entities = M.insert c1 (m {lifePoints = 1}) (M.insert c2 (pl {lifePoints = 1}) ents)}
  | otherwise = 
    lvl {entities = M.delete c1 (M.insert c2 (pl {lifePoints = lp2 - (lp1 `div` 2) }) ents)}

entityInteraction lvl@(Level _ ents _ _) c1 m1@(Monster _ _ _) c2 m2@(Monster _ _ _) = 
  lvl {entities = M.insert c1 m2 (M.insert c2 m1 ents) }

entityInteraction lvl  _ _ _ _ = lvl

{-
  ramasser un objet
-}
pickup :: Level -> Coord -> Coord -> Entity -> Obj -> Level
pickup lvl@(Level _ ents lvlobjs _) oldpos newpos pl@(Player _ plobjs _ _) (Key) =
  lvl {objectsLvl = M.delete newpos lvlobjs, 
      entities = M.delete oldpos (M.insert newpos (pl {objects = (Key:plobjs)}) ents)}

pickup lvl@(Level _ ents lvlobjs _) oldpos newpos pl@(Player _ plobjs _ _) (Torch) =
  lvl {objectsLvl = M.delete newpos lvlobjs, 
      entities = M.delete oldpos (M.insert newpos (pl {objects = (Torch:plobjs)}) ents)}

pickup lvl@(Level _ ents lvlobjs _) oldpos newpos pl@(Player lp _ _ _) (LifePotion x) =
  lvl {objectsLvl = M.delete newpos lvlobjs,
      entities = M.delete oldpos (M.insert newpos (pl {lifePoints = (lp + x)}) ents)}

pickup lvl _ _ _ _ = lvl


moveEntity :: Level -> Coord -> Entity -> Level
{-
  Bouger un joueur
-}
moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Player _ _ (MLeft:t) _)
  | px==0 || (isWall (px-1) py cnt) = 
    lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord (px-1) py) ents) of
      Just x -> entityInteraction lvl coord ent (Coord (px-1) py) x 
      Nothing -> 
        (case (M.lookup (Coord (px-1) py) objs) of
          Just y -> pickup lvl coord (Coord (px-1) py) (ent {movements = t}) y
          Nothing -> lvl {entities = M.delete coord (M.insert (Coord (px-1) py) (ent {movements = t}) ents)})



moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py)   ent@(Player _ _ (MRight:t) _)
  | px==(l-1) || (isWall (px+1) py cnt) = 
    lvl { entities = M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord (px+1) py) ents) of
      Just x -> entityInteraction lvl coord ent (Coord (px+1) py) x 
      Nothing ->
        (case (M.lookup (Coord (px+1) py) objs) of
          Just y -> pickup lvl coord (Coord (px+1) py) (ent {movements = t}) y
          Nothing -> lvl {entities = M.delete coord (M.insert (Coord (px+1) py) (ent {movements = t}) ents)}) 


moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py)   ent@(Player _ _ (MUp:t) _)
  | py==0 || (isWall px (py-1) cnt) =
    lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord px (py-1) ) ents) of
      Just x -> entityInteraction lvl coord ent (Coord px (py-1) ) x
      Nothing ->
        (case (M.lookup (Coord px (py-1)) objs) of
          Just y -> pickup lvl coord (Coord px (py-1)) (ent {movements = t}) y
          Nothing -> lvl {entities = M.delete coord (M.insert (Coord px (py-1)) (ent {movements = t}) ents)})

moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Player _ _ (MDown:t) _)
  | py==(w-1) || (isWall px (py + 1) cnt) = 
    lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord px (py+1) ) ents) of
      Just x -> entityInteraction lvl coord ent (Coord px (py+1) ) x
      Nothing ->
        (case (M.lookup (Coord px (py+1)) objs) of
          Just y -> pickup lvl coord (Coord px (py+1)) (ent {movements = t}) y
          Nothing -> lvl {entities = M.delete coord (M.insert (Coord px (py+1)) (ent {movements = t}) ents)})

moveEntity lvl@(Level _ ents _ _) coord ent@(Player _ _ (Stay:t) _) = 
  lvl {entities = M.insert coord (ent {movements = t}) ents}
{-
  Bouger un monster
-}
moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Monster _ _ (MLeft:t))
  | px==0 || (isWall (px-1) py cnt) =
     lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord (px-1) py) ents) of
      Just x -> entityInteraction lvl coord ent (Coord (px-1) py) x 
      Nothing -> lvl {entities = M.delete coord (M.insert (Coord (px-1) py) (ent {movements = t}) ents)}

moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Monster _ _ (MRight:t))
  | px==(l-1) || (isWall (px+1) py cnt) = 
     lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord (px+1) py) ents) of
      Just x -> entityInteraction lvl coord ent (Coord (px+1) py) x 
      Nothing -> lvl {entities = M.delete coord (M.insert (Coord (px+1) py) (ent {movements = t}) ents)}

moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Monster _ _ (MUp:t))
  | py==0 || (isWall px (py-1) cnt) =
    lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord px (py-1) ) ents) of
      Just x -> entityInteraction lvl coord ent (Coord px (py-1)) x 
      Nothing -> lvl {entities = M.delete coord (M.insert (Coord px (py-1)) (ent {movements = t}) ents)}

moveEntity lvl@(Level ct@(Carte l w cnt) ents objs stdgen) coord@(Coord px py) ent@(Monster _ _ (MDown:t)) 
  | py==(w-1) || (isWall px (py + 1) cnt) = 
    lvl { entities =  M.insert coord (ent {movements = t}) ents } 
  | otherwise = 
    case (M.lookup (Coord px (py+1) ) ents) of
      Just x -> entityInteraction lvl coord ent (Coord px (py+1)) x 
      Nothing -> lvl {entities = M.delete coord (M.insert (Coord px (py+1)) (ent {movements = t}) ents)}

moveEntity lvl@(Level _ ents _ _) c ent@(Monster _ _ (Stay:t)) = lvl {entities = M.insert c (ent {movements = t}) ents }

moveEntity lvl@(Level _ ents _ _) c ent =  lvl {entities = M.insert c ent ents}



{-
    Bouger une liste d'entités dans un niveau
-}
moveEntities :: Level -> Level
moveEntities lvl@( Level gmap1 ents1 objs stdgen) =
  M.foldrWithKey (\k x clvl -> moveEntity clvl k x) lvl ents1


{-
    Récuperer le mouvement à effectuer pour atteindre une coordonnée
-}
coordToMov :: Coord -> Coord -> Movement
coordToMov (Coord x1 y1) (Coord x2 y2)
  | x1<x2 && y1==y2 = MRight
  | x1>x2 && y1==y2 = MLeft
  | x1==x2 && y1>y2 = MUp
  | x1==x2 && y1<y2 = MDown
  | otherwise = Stay


{-
  Retourne la liste des mouvements possibles pour une entité positionnée dans (Coord px py)
-}
possibleMovements :: Coord -> Level -> [Movement] 
possibleMovements (Coord px py) lvl@(Level (Carte l w cnt) ents objs stdgen)  =
  ( 
    (if px > 0 && (isAccessible lvl (Coord (px-1) py) )  
      then [MLeft]
      else [])
    ++
    (if px < (l-1) && (isAccessible lvl (Coord (px+1) py) ) 
      then [MRight]
      else [])
    ++
    (if py > 0 && (isAccessible lvl (Coord px (py-1)) ) 
      then [MUp]
      else [])
    ++
    (if py < (w-1) && (isAccessible lvl (Coord px (py+1)) ) 
      then [MDown]
      else [])
    ++ []
  )


{-
  Seelctionne les keycodes qui sont possibles en fonction de la destination
  exp : 
    si on est en [0;0] et 'movements'=[MRight,MRight,MRight] et que [0;3] contient un mur
    et qu'on appuie sur "D" (la touche pour bouger à droite) on ajoute pas un (MRight) à 'movements'
-}
selectKeycodes :: [Keycode] -> Coord -> Carte -> [Keycode] -> (Coord,[Keycode])
selectKeycodes (KeycodeZ:kt) c@(Coord px py) carte@(Carte w l cont) kbout
  | py>0 && ( not (isWall px (py-1) cont) ) = selectKeycodes kt (Coord px (py-1)) carte (KeycodeZ:kbout)
  | otherwise = selectKeycodes kt c carte kbout

selectKeycodes (KeycodeS:kt) c@(Coord px py) carte@(Carte w l cont) kbout
  | py<l-1 && ( not (isWall px (py+1) cont)) = selectKeycodes kt (Coord px (py+1)) carte (KeycodeS:kbout)
  | otherwise = selectKeycodes kt c carte kbout

selectKeycodes (KeycodeQ:kt) c@(Coord px py) carte@(Carte w l cont) kbout
  | px>0 && (not (isWall (px-1) py cont)) = selectKeycodes kt (Coord (px-1) py) carte (KeycodeQ:kbout)
  | otherwise = selectKeycodes kt c carte kbout

selectKeycodes (KeycodeD:kt) c@(Coord px py) carte@(Carte w l cont) kbout
  | px<w-1 && (not (isWall (px+1) py cont)) = selectKeycodes kt (Coord (px+1) py) carte (KeycodeD:kbout)
  | otherwise = selectKeycodes kt c carte kbout

selectKeycodes _ c _ kbout = (c,kbout)


{-
  Choisi les mouvements d'une entité,
  si c'est un 'Player' on traite la liste des cliques pour trouver les mouvements choisis
  sinon on choisi aleatoirement des mouvements
-}
decideEntity :: Coord -> Entity -> [Keycode] -> Level -> Level

decideEntity coord pl@(Player _ _ _ dest) kc (Level gmap ents objs stdgen)  = 
  Level gmap (M.insert coord 
    (addMovements pl 
      (map (\x -> 
        case x of 
          KeycodeZ -> MUp  
          KeycodeD -> MRight
          KeycodeQ -> MLeft
          KeycodeS -> MDown
          otherwise -> Stay
          ) kc 
      ) 
    ) ents) objs stdgen

decideEntity coord m@(Monster _ _ _) _ lvl@(Level gmap ents objs stdgen) = 
  case (possibleMovements coord lvl) of
    l@(h:t) -> 
      (\(ind,new_stdgen) -> 
        Level gmap (
      M.insert coord
        (addMovement m ( l !! ind) )
        ents) objs new_stdgen ) (Rand.randomR (0, (L.length l)-1) stdgen)
    [] -> lvl
        
       
{-
  choisir les mouvements des entités dans le niveau 
-}
decide :: Keyboard -> Level -> Level
decide kb (Level gmap1 ents1 objs stdgen)  = 
  M.foldrWithKey 
    (\k x lvl -> 
      case x of
        pl@(Player _ _ _ _) -> 
          (\(dest_bis,kbrd_bis) ->  
            decideEntity k (pl {dest = dest_bis}) kbrd_bis lvl)  (selectKeycodes (S.toList kb) k gmap1 [])
        
        _ -> decideEntity k x [] lvl
    )
    (Level gmap1 (M.fromList []) objs stdgen) 
      ents1 

{-
  Chercher une clé dans une liste d'objets
-}
hasKey :: [Obj] -> Bool
hasKey (Key:objs) = True 
hasKey (_:objs) = hasKey objs
hasKey [] = False

{-
  Vérifier si un joueur a collecté une clé (pour pouvoir quitter le labyrinthe)
-}
playerHasKey :: Entity -> Bool
playerHasKey (Player _ objs _ _) = if (hasKey objs) then True else False 


{-
  fait passer le joeur d'un niveau à un niveau supérieur
-}
changePlayersLevel :: Entity -> Int -> Int -> Int -> [Level] -> [Level] -> (Int, [Level])
changePlayersLevel player src dest n lvls@(lvl@(Level (Carte  _ _ cont) ents objs stdgen):t) retlvls
  | src == dest = (src,lvls)

  | n == src && dest > src = 
    changePlayersLevel player src dest (n+1) t (retlvls ++ [(popEntity player lvl)])

  | n == src && dest < src =
    (dest, retlvls ++ ((popEntity player lvl):t) )

  | n == dest && src > dest =
    changePlayersLevel player src dest (n+1) t (retlvls ++ [(insertEntity (getInDoorCoord (M.toList cont)) lvl player)])

  | n == dest && src < dest =
    (dest, retlvls ++ ((insertEntity (getInDoorCoord (M.toList cont)) lvl player):t) )

  | otherwise = (src,lvls)



{-
  choisi les mouvements des entités du niveau courant 
-}
doDecide :: Keyboard -> Int -> Int -> [Level] -> [Level]
doDecide kb n clvl (h:t)
  | n==clvl = (decide kb h):t
  | n<clvl = h:(doDecide kb (n+1) clvl t)
  | otherwise = error "Erreur1 dans : doDecide"
doDecide kb n clvl [] = error "Erreur2 dans : doDecide"


{-
  bouge les entités du nieau courant
-}
doMove ::  Int -> Int -> [Level] -> [Level]
doMove n clvl (h:t) 
  | n==clvl = (moveEntities h):t
  | n<clvl = h:(doMove (n+1) clvl t)
  | otherwise = error "Erreur1 dans : doMove"
doMove  n clvl []  = error "Erreur2 dans : doMove"


hasNoMonstersEnts :: [(Coord,Entity)] -> Bool
hasNoMonstersEnts ((_,(Monster _ _ _)):ents) = False 
hasNoMonstersEnts ((_,(Player _ _ _ _)):ents) = hasNoMonstersEnts ents 
hasNoMonstersEnts [] = True 

hasNoMonstersLvl :: Level -> Bool
hasNoMonstersLvl (Level _ ents _ _) = hasNoMonstersEnts (M.toList ents)

hasNoMonstersLvls :: [Level] -> Bool 
hasNoMonstersLvls (lvl1:lvls) = 
  if (hasNoMonstersLvl lvl1) 
  then (hasNoMonstersLvls lvls)
  else False
hasNoMonstersLvls [] = True 

hasNoMonsters :: GameState -> Bool
hasNoMonsters (GameState _ lvls _ ) = hasNoMonstersLvls lvls
{-
    Etape de jeu 
-}
gameStep :: RealFrac a => GameState -> Keyboard -> a -> GameState
gameStep gamestate@(GameState clvl lvls _ ) kb deltaTime = 
  if( hasNoMonsters gamestate ) 
  then (GameState clvl lvls Won )
  else 
  
  (\gs@(GameState clvl lvls  _) -> (
  case (getPlayerFromLevel (lvls !! clvl)) of
    Just ((Coord px py),ent) -> (
      case (getCase px py (getCarteMap (lvls !! clvl)) ) of
        Just Out ->    if(playerHasKey ent)  then
          (\(newclvl,newlvls) -> 
            gs {currentLevel = newclvl, levels = newlvls } ) 
            (changePlayersLevel ent clvl ( (clvl+1) `mod` (L.length lvls)) 0 lvls [])
            else gs
        _ -> gs
      )
    Nothing ->(GameState clvl lvls Lost )) )
    (gamestate {levels = doMove 0 clvl (doDecide kb 0 clvl lvls)} )



